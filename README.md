




For Development:

# zegers-website

## About

Personal Website showcasing Work and blogposts, as well development playground to try out new css animations.
Hosted on Netlify, CI/CD pipeline set up to automatically deploy when merged to Master, pipeline tests run when merged to develop branch

Simple frontend build using nuxt.js and markdown for blogposts. 

## Build Setup for Development

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
