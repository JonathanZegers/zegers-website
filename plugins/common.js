import Vue from 'vue'

Vue.mixin({
  methods: {
    getClassType() {
      const c = ['default', 'primary', 'info', 'success', 'danger', 'warning']
      return c[Math.floor(Math.random() * c.length)]
    },
  },
})
